Follow the tux
=================
* http://www.raywenderlich.com/24155/how-to-make-a-side-scrolling-beat-em-up-game-like-scott-pilgrim-with-cocos2d-part-1
* http://www.raywenderlich.com/24452/how-to-make-a-side-scrolling-beat-em-up-game-like-scott-pilgrim-with-cocos2d-part-2


Bug
=====
--robot is so stiff when it move (Gamelayer.cpp : 308)--


Todo list
===========

An Exciting Announcement
-----------------------------

If you\' re craving more Beat \'Em Up action, I have good news for you �C I will soon be releasing a Beat \'Em Up Starter Kit here at raywenderlich.com!
------------------------------------------------------------

* You\' re probably wondering just how awesome it will be. As a teaser, here��s a list of what you can expect to find and learn in the kit:
* More actions! 3-hit combos, jumping, running
* Combination actions: jumping attack, running attack
* Adding Life Bars
* Animated 8-directional D-pad
* Animated D-pad buttons
* State machine applied to game events, and battle events with enemy spawning!
* Multiple levels, and using PLIST files for setting up events
* Creating different types of enemies using the sprite color tinting technique
* Boss enemy: Pompadour VS Mohawk! (cue dramatic music)
* More advanced enemy AI
* Much better collision system using circles instead of rectangles
* Adjusting collision circles based on actions, and drawing these circles onto the screen for debugging
* A weapon for your Pompadoured Hero: the Gauntlet
* Destructible tiled map objects
* Visible damage numbers and hit explosions
* ... and much more!
