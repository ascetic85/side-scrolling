#pragma once

#include "Defines.h"

class ActionSprite : public CCSprite
{
    // actions
    // these are the CCActions (Cocos2D actions) to be executed for each state. 
    // The CCActions will be a combination of executing sprite animations and 
    // other events triggered when the character switches states.
    CC_SYNTHESIZE_RETAIN(CCAction*, m_idleAction, IdleAction);
    CC_SYNTHESIZE_RETAIN(CCAction*, m_attackAction, AttackAction);
    CC_SYNTHESIZE_RETAIN(CCAction*, m_walkAction, WalkAction);
    CC_SYNTHESIZE_RETAIN(CCAction*, m_hurtAction, HurtAction);
    CC_SYNTHESIZE_RETAIN(CCAction*, m_knockedOutAction, KnockedOutAction);

    //this will represent the body of the sprite.
    CC_SYNTHESIZE_PASS_BY_REF(BoundingBox, m_hitBox, HitBox); 

    // this will represent the hand of the sprite.
    CC_SYNTHESIZE_PASS_BY_REF(BoundingBox, m_attackBox, AttackBox);

    // states
    // holds the current action/state of the sprite, using a type named 
    // ActionState that you will define soon.
    CC_SYNTHESIZE(ActionState, m_actionState, ActionState);

    // attributes
    // contains values for the sprite��s walk speed for movement, hit/health 
    // points for getting hurt, and damage for attacking.
    CC_SYNTHESIZE(float, m_walkSpeed, WalkSpeed);
    //CC_SYNTHESIZE(float, m_hitPoints, HitPoints);

public:
    float getHitPoints(){return m_hitPoints;}
    float getHitPointsConst(){return m_hitPointsConst;}
    void setHitPoints(float v) {m_hitPoints=v; m_hitPointsConst=v;}
private:
    float m_hitPoints;
    float m_hitPointsConst;

    CC_SYNTHESIZE(float, m_damage, Damage);
    
    // movement
    // will be used later to calculate how the sprite moves around the map.
    CC_SYNTHESIZE(CCPoint, velocity, Velocity);
    CC_SYNTHESIZE(CCPoint, desiredPos, DesiredPos);

    // measurements
    // holds useful measurement values regarding the actual image of the sprite.
    // You need these values because the sprites you will use have a canvas size 
    // that is much larger than the image contained inside.
    CC_SYNTHESIZE(float, centerToSides, CenterToSides);
    CC_SYNTHESIZE(float, centerToBottom, centerToBottom);

public:
    ActionSprite() ;
    ~ActionSprite();

    // from parent
    CREATE_FUNC(ActionSprite);

    // action methods
    // you won��t call the CCActions (from the actions section above) directly. 
    // Instead, you will be using these methods to trigger each state.
    void idle();
    void attack();
    void hurtWithDamage(float damage);
    void knockout();
    void walkWithDirection(CCPoint direction);

    // scheduled
    // anything that needs to run constantly at certain intervals, such as updates 
    // to the Sprite��s position and velocity, will be put here.
    void update(float dt);

    // 
    void setPosition(CCPoint position);

    // 
    BoundingBox createBoundingBoxWithOrigin(CCPoint origin, CCSize size);

    void transformBoxes();
};