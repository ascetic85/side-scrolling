#include "JoyStick.h"
#include "CCHelper.h"

JoyStick::JoyStick(void)
{
}

JoyStick::~JoyStick(void)
{
}

void JoyStick::updatePos(float dt)
{
	jsSprite->setPosition(ccpAdd(jsSprite->getPosition(),ccpMult(ccpSub(currentPoint, jsSprite->getPosition()),0.5)));
}

//����ҡ��
void JoyStick::Active()
{
	if (!active) {
		active=true;
		schedule(schedule_selector(JoyStick::updatePos));//����ˢ�º���
		gTouchDispatcher->addTargetedDelegate(this, 0, false); //���Ӵ���ί��
	}else {
	}
}
//���ҡ��
void   JoyStick::Inactive()
{
	if (active) {
		active=false;
		this->unschedule(schedule_selector(JoyStick::updatePos));//ɾ��ˢ��
		gTouchDispatcher->removeDelegate(this);//ɾ��ί��
	}else {
	}
}

//ҡ�˷�λ
CCPoint JoyStick::getDirection()
{
	return ccpNormalize(ccpSub(centerPoint, currentPoint));
}

//ҡ������
float JoyStick::getVelocity()
{
	return ccpDistance(centerPoint, currentPoint);
}

JoyStick* JoyStick::JoyStickWithCenter(CCPoint aPoint 
									, float aRadius 
									, CCSprite* aJsSprite
									, CCSprite* aJsBg
									, bool _isFollowRole)
{
	JoyStick *jstick = JoyStick::create();
	jstick->initWithCenter(aPoint,aRadius,aJsSprite,aJsBg,_isFollowRole);
	return jstick;
}

bool JoyStick::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	if (!active)
		return false;
	this->setVisible(true);
	//CCPoint touchPoint = touch->locationInView(touch->view());
	//touchPoint = CCDirector:: sharedDirector()->convertToGL(touchPoint);
	CCPoint touchPoint = touch->getLocation();
	if(!isFollowRole)
	{
		if (ccpDistance(touchPoint, centerPoint) > radius)
		{
			return false;
		}
	}
	currentPoint = touchPoint;
	if(isFollowRole)
	{
		centerPoint=currentPoint;
		jsSprite->setPosition(currentPoint);
		this->getChildByTag(88)->setPosition(currentPoint);
	}
	return true;
}

void  JoyStick::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	//CCPoint touchPoint = touch->locationInView(touch->view());
	//touchPoint = CCDirector:: sharedDirector()->convertToGL(touchPoint);
	CCPoint touchPoint = touch->getLocation();
	if (ccpDistance(touchPoint, centerPoint) > radius)
	{
		currentPoint = ccpAdd(centerPoint,ccpMult(ccpNormalize(ccpSub(touchPoint, centerPoint)), radius));
	}
	else
	{
		currentPoint = touchPoint;
	}
}
void  JoyStick::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	currentPoint = centerPoint;
	if(isFollowRole)
		this->setVisible(false);
}

JoyStick* JoyStick::initWithCenter(CCPoint aPoint 
													, float aRadius 
													, CCSprite* aJsSprite
													, CCSprite* aJsBg
													, bool _isFollowRole)
{
	isFollowRole =_isFollowRole;
	active = false;
	radius = aRadius;
	if(!_isFollowRole)
		centerPoint =aPoint;
	else
		centerPoint =ccp(0,0);

	currentPoint = centerPoint;
	jsSprite = aJsSprite;
	jsSprite->setPosition(centerPoint);
	aJsBg->setPosition(centerPoint);
	aJsBg->setTag(88);
	this->addChild(aJsBg);
	this->addChild(jsSprite);
	if(isFollowRole)
		this->setVisible(false);
	
	this->Active();//����ҡ��
	return this;
}

SimpleDPad * SimpleDPad::create( const char *fileName, float radius )
{
    SimpleDPad *dpad = new SimpleDPad();
    if (dpad && dpad->initWithFile(fileName, radius))
    {
        dpad->autorelease();
        return dpad;
    }
    CC_SAFE_DELETE(dpad);
    return NULL;
}

bool SimpleDPad::initWithFile(const char *fileName, float radius)
{
    bool bRet = false;
    do 
    {
        CC_BREAK_IF(!CCSprite::initWithFile(fileName));

        m_radius = radius;
        m_direction = ccp(0, 0);
        m_isHeld = false;

        scheduleUpdate();   // call update()
        bRet = true;
    } while (0);
    return bRet;
}

void SimpleDPad::onEnterTransitionDidFinish()
{
    gTouchDispatcher->addTargetedDelegate(this, 1, true);
}

void SimpleDPad::onExit()
{
    gTouchDispatcher->removeDelegate(this);
}

void SimpleDPad::update( float dt )
{
    if (m_isHeld)
    {
#if USING_FD
        isHoldingDirectionDelegate(this, m_direction);
#else
        m_delegate->simpleDPadIsHoldingDirection(this, m_direction);
#endif
    }
}

bool SimpleDPad::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
    CCPoint location = pTouch->getLocation();

    float distanceSQ = ccpDistanceSQ(location, getPosition());
    if (distanceSQ <= m_radius * m_radius)
    {
        // get angle 8 directions
        updateDirectionForTouchLocation(location);
        m_isHeld = true;
    }
    return m_isHeld;
}

void SimpleDPad::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{
    updateDirectionForTouchLocation(pTouch->getLocation());
}

void SimpleDPad::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{
    m_direction = ccp(0, 0);
    m_isHeld = false;

#if USING_FD
    dpadTouchEnded(this);
#else
    m_delegate->simpleDPadTouchEnded(this);
#endif
}

// calculates the location of the touch against the center of the D-pad 
// (the center(anchor point) must be (.5, .5) , assigns the correct value
// for direction based on the resulting angle, and passes the value onto
// the delegate. The angle values, in degrees, may look weird to you, 
// since the common misconception is that 0 degrees means north. 
// Actually, 0 degrees in mathematics is east of the circle, becoming 
// positive in the counterclockwise direction. Since you multiply the angle 
// by -1, it becomes positive in the clockwise direction, 
// which is what I��m used to working with.
void SimpleDPad::updateDirectionForTouchLocation( CCPoint location )
{
    float radians = ccpToAngle(ccpSub(location, getPosition()));
    float degrees = -1 * CC_RADIANS_TO_DEGREES(radians);
    if (degrees >= -22.5 && degrees <= 22.5)
    {
        // right
        m_direction = ccp(1, 0);
    }
    else if (degrees > 22.5 && degrees < 67.5)
    {
        // bottom right
        m_direction = ccp(1, -1);
    }
    else if (degrees >= 67.5 && degrees <= 112.5)
    {
        // bottom
        m_direction = ccp(0, -1);
    }
    else if (degrees > 112.5 && degrees < 157.5)
    {
        // bottom left
        m_direction = ccp(-1, -1);
    }
    else if (degrees >= 157.5 || degrees <= -157.5)
    {
        // left
        m_direction = ccp(-1, 0);
    }
    else if (degrees < -22.5 && degrees > -67.5) 
    {
        //top right
        m_direction = ccp(1.0, 1.0);
    } else if (degrees <= -67.5 && degrees >= -112.5) 
    {
        //top
        m_direction = ccp(0.0, 1.0);
    } else if (degrees < -112.5 && degrees > -157.5) 
    {
        //top left
        m_direction = ccp(-1.0, 1.0);
    }
    
#if USING_FD
    didChangeDirectionToDelegate(this, m_direction);
#else
    m_delegate->simpleDPadDidChangeDirectionTo(this, m_direction);
#endif
}
