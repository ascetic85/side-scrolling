#pragma once

#include "cocos2d.h"
USING_NS_CC;

#define gDataMrg LevelDataMgr::sharedMrg()

// level*.plist 的格式
// values {} 还没有想好
// leveldata {} 游戏的相关设置, 音乐, 图片, 游戏数值...
class LevelDataMgr
{
    CC_SYNTHESIZE_RETAIN(CCDictionary*, m_levelDict, LevelDict);

public:
    ~LevelDataMgr(void);

    static LevelDataMgr* sharedMrg();

    void readHero(const char *hero);
    void readLevel(int level);
    const char *cstringForKeyInLevelData(const char *key);

    const CCString *valueForKey(const char *key);
    const char *cstringForKeyOfHero(const char *key);

protected:
    LevelDataMgr(void);
    CCDictionary* m_heroDict;
};

