#pragma once

#include "cocos2d.h"
USING_NS_CC;

class SimpleDPad;
class HudLayer : public CCLayer
{
    CC_SYNTHESIZE(SimpleDPad*, m_dpad, DPad);
public:
    HudLayer(void);
    ~HudLayer(void);

    virtual bool init();
    CREATE_FUNC(HudLayer);
};

