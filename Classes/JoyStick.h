#pragma once

// This JoyStick comes from http://www.himigame.com/iphone-cocos2dx/721.html
// SimpleDPad comes from http://www.raywenderlich.com/24155/how-to-make-a-side-scrolling-beat-em-up-game-like-scott-pilgrim-with-cocos2d-part-1
// 2012-11-07 

#include "cocos2d.h"
#include "FastDelegate.h"
#include "FastDelegateBind.h"

using namespace fastdelegate;
USING_NS_CC;

class SimpleDPad;

//#define USING_FD 0

class SimpleDPadDelegate
{
public:
    virtual void simpleDPadDidChangeDirectionTo(SimpleDPad *dpad, CCPoint direction) = 0;
    virtual void simpleDPadIsHoldingDirection(SimpleDPad *dpad, CCPoint direction) = 0;
    virtual void simpleDPadTouchEnded(SimpleDPad *dpad) = 0;
};

class SimpleDPad : public CCSprite , public CCTouchDelegate
{
    // is the delegate of the D-pad
    CC_SYNTHESIZE(SimpleDPadDelegate*, m_delegate, Delegate);

    // is a Boolean that returns YES as long as the player is touching the D-pad.
    CC_SYNTHESIZE(bool, m_isHeld, IsHeld); 

public:

    static SimpleDPad *create(const char *fileName, float radius);
    
    virtual bool initWithFile(const char *fileName, float radius);

    // myself
#if USING_FD
    typedef FastDelegate2<SimpleDPad*, CCPoint, void> DidChangeDirectionToDelegate;
    typedef FastDelegate2<SimpleDPad*, CCPoint, void> IsHoldingDirectionDelegate;
    typedef FastDelegate1<SimpleDPad*> DPadTouchEndedDelegate;

    DidChangeDirectionToDelegate didChangeDirectionToDelegate;
    IsHoldingDirectionDelegate isHoldingDirectionDelegate;
    DPadTouchEndedDelegate  dpadTouchEnded;
#endif

protected:
    // from parent
    void onEnterTransitionDidFinish();
    void onExit();
    void update(float dt);
    bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
    void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);

    // myself
    void updateDirectionForTouchLocation(CCPoint location);

protected:
    float m_radius;     // is simply the radius of the circle formed by the D-pad.
    CCPoint m_direction;// is the current direction being pressed. This is a vector 
                        // with (-1.0, -1.0) being the bottom left direction, 
                        // and (1.0, 1.0) being the top right direction.

};

class JoyStick :public CCLayer {

public :
	JoyStick();
	~JoyStick();

	//��ʼ�� aPoint��ҡ������ aRadius��ҡ�˰뾶 aJsSprite��ҡ�˿��Ƶ� aJsBg��ҡ�˱���
	static JoyStick*  JoyStickWithCenter(CCPoint aPoint 
										,float aRadius 
										,CCSprite* aJsSprite
										,CCSprite* aJsBg
										,bool _isFollowRole);

	//����ҡ��
	void Active();

	//���ҡ��
	void Inactive();

private:
	JoyStick * initWithCenter(CCPoint aPoint
							, float aRadius 
							, CCSprite* aJsSprite
							, CCSprite* aJsBg
							, bool _isFollowRole);

	CCPoint centerPoint;//ҡ������

	CCPoint currentPoint;//ҡ�˵�ǰλ��

	bool active;//�Ƿ񼤻�ҡ��

	float radius;//ҡ�˰뾶

	CCSprite *jsSprite;

	bool isFollowRole;//�Ƿ�����û����

	CCPoint getDirection();

	float getVelocity();

	void  updatePos(float dt);

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	CREATE_FUNC(JoyStick);
};
