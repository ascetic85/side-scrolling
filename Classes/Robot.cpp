#include "Robot.h"
#include "CCHelper.h"

bool Robot::init()
{
    bool bRet = false;
    do 
    {
        CC_BREAK_IF(! initWithSpriteFrameName("robot_idle_00.png"));

        m_nextDecisionTime = 0;

        setIdleAction(CCH::foreverAnimateHelper("robot_idle", 5, 1.0/12.0));
        setAttackAction(CCSequence::create(CCH::animateHelper("robot_attack", 5, 1.0/24.0)
                            , CCCallFunc::create(this, callfunc_selector(Robot::idle))
                            , NULL)
                        );
        setWalkAction(CCH::foreverAnimateHelper("robot_walk", 6, 1.0/12.0));
        setHurtAction(CCSequence::create(CCH::animateHelper("robot_hurt", 3, 1.0/12.0)
            , CCCallFunc::create(this, callfunc_selector(Robot::idle))
            , NULL));
        setKnockedOutAction(CCSequence::create(
            CCH::animateHelper("robot_knockout", 5, 1.0/12.0)
            , CCBlink::create(2.0, 10.0)
            , NULL
            ));

        CCDictionary *dict = CCDictionary::createWithContentsOfFile("robot.plist");
        
        setWalkSpeed(dict->valueForKey("walkSpeed")->floatValue());
        setcenterToBottom(dict->valueForKey("centerToBottom")->floatValue());
        setCenterToSides(dict->valueForKey("centerToSides")->floatValue());
        setHitPoints(dict->valueForKey("hitPoints")->floatValue());
        setDamage(dict->valueForKey("damage")->floatValue());

        setHitBox(createBoundingBoxWithOrigin(ccp(-getCenterToSides(), -getcenterToBottom())
                            , CCSizeMake(getCenterToSides()*2, getcenterToBottom()*2)));
        setAttackBox(createBoundingBoxWithOrigin(ccp(-getCenterToSides(), -5)
                            , CCSizeMake(25, 20)));
        bRet = true;
    } while (0);
    return bRet;
}
