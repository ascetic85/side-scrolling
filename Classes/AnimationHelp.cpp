#include "AnimationHelp.h"

cocos2d::CCArray* framesArrayHelper( const char *name, int capacity )
{
	CCArray *frames = CCArray::create();
	for (int i = 0; i< capacity; i++)
	{
		CCString *idex = CCString::createWithFormat("%s_%02d.png", name, i);
		CCSpriteFrame *frame = CCSpriteFrameCache::sharedSpriteFrameCache()
			->spriteFrameByName(idex->getCString());
		frames->addObject(frame);
	}
	return frames;
}

CCAnimation *animationHelper(const char *name, int capacity, float delay)
{
	return CCAnimation::createWithSpriteFrames(framesArrayHelper(name, capacity)
											   , delay);
}

CCAnimate * animateHelper( const char *name, int capacity, float delay )
{
	return CCAnimate::create(animationHelper(name, capacity, delay));
}

CCRepeatForever * foreverAnimateHelper( const char *name, int capacity, float delay )
{
	return CCRepeatForever::create(animateHelper(name, capacity, delay));
}

