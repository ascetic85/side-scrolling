#include "GameLayer.h"
#include "SimpleAudioEngine.h"

#include "GameScene.h"
#include "LevelDataMgr.h"
#include "CCHelper.h"
#include "Hero.h"
#include "Robot.h"

using namespace CocosDenshion;

GameLayer::GameLayer(void)
    : m_tilemap(0)
    , m_actors(0)
    , m_hero(0)
    , m_robots(0)
{
}


GameLayer::~GameLayer(void)
{
    unscheduleUpdate();
}

bool GameLayer::initWithLevel(int level)
{
    bool bRet = false;
    do 
    {
        setTouchEnabled(true);

        // init data
        setLevel(level);
        gDataMrg->readHero("hero.plist");
        gDataMrg->readLevel(level);

        loadMusic();

        // load tile map
        initTilemap();

        // load global images
        gSpriteFrameCache->addSpriteFramesWithFile(
                gDataMrg->cstringForKeyInLevelData("pd_sprites"));
        m_actors = CCSpriteBatchNode::create(
                gDataMrg->cstringForKeyInLevelData("pd_sprites_pvr"));
        m_actors->getTexture()->setAliasTexParameters();
        addChild(m_actors, -5);

        // init hero
        initHero();

        // follow hero
        runAction(CCFollow::create(m_hero, CCRectMake(0, 0, m_tilemap->getMapSize().width*m_tilemap->getTileSize().width
            , m_tilemap->getMapSize().width*m_tilemap->getTileSize().height)));

        // init robots
        initRobots();

        // update 
        scheduleUpdate();

        bRet = true;
    } while (0);
    return bRet;
}

GameLayer* GameLayer::create( int level )
{
    GameLayer *layer = new GameLayer();
    if (layer && layer->initWithLevel(level))
    {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return NULL;
}

void GameLayer::initTilemap()
{
    m_tilemap = CCTMXTiledMap::create(gDataMrg->cstringForKeyInLevelData("pd_tilemap"));

    addChild(m_tilemap, -6);
}

void GameLayer::initHero()
{
    // add hero
    m_hero = Hero::create();
    CC_ASSERT(m_hero);

    //addChild(m_hero);
    m_actors->addChild(m_hero); // hero and robots are at the same node
    m_hero->setPosition(CCPointFromString(gDataMrg->cstringForKeyOfHero("position")));
    m_hero->setDesiredPos(m_hero->getPosition());

    m_hero->idle();
}

void GameLayer::ccTouchesBegan( CCSet *pTouches, CCEvent *pEvent )
{
    CC_ASSERT(m_hero);
    m_hero->attack();

    if (m_hero->getActionState() == kActionStateAttack) 
    {
        Robot *robot;
        CCObject *obj;
        CCARRAY_FOREACH(m_robots, obj) 
        {
            robot = (Robot *) obj;
            if (robot->getActionState() != kActionStateKnockedOut) 
            {
                if (fabsf(m_hero->getPositionY() - robot->getPositionY()) < 10) 
                {
                    if (CCRect::CCRectIntersectsRect(m_hero->getAttackBox().actual
                                , robot->getHitBox().actual)) 
                        robot->hurtWithDamage(m_hero->getDamage());
                }
            }
        }
    }
}

void GameLayer::simpleDPadDidChangeDirectionTo( SimpleDPad *dpad, CCPoint direction )
{
    CC_ASSERT(m_hero);
    m_hero->walkWithDirection(direction);
}

void GameLayer::simpleDPadIsHoldingDirection( SimpleDPad *dpad, CCPoint direction )
{
    CC_ASSERT(m_hero);
    m_hero->walkWithDirection(direction);
}

void GameLayer::simpleDPadTouchEnded( SimpleDPad *dpad )
{
    CC_ASSERT(m_hero);
    if (m_hero->getActionState() == kActionStateWalk)
    {
        m_hero->idle();
    }
}

void GameLayer::update( float dt )
{
    CC_ASSERT(m_hero);
    m_hero->update(dt);
    
    //
    updatePositions();

    //
    updateRobots(dt);

    setViewPointCenter(m_hero->getPosition());
}

void GameLayer::updatePositions()
{
    CC_ASSERT(m_hero);

    float posx = MIN(m_tilemap->getMapSize().width * m_tilemap->getTileSize().width - m_hero->getCenterToSides()
        , MAX(m_hero->getCenterToSides(), m_hero->getDesiredPos().x) );
    float posy = MIN(3 * m_tilemap->getTileSize().height + m_hero->getcenterToBottom()
        , MAX(m_hero->getcenterToBottom(), m_hero->getDesiredPos().y));
    m_hero->setPosition(ccp(posx, posy));

    // Update robots
    Robot *robot;
    CCObject *obj;
    CCARRAY_FOREACH(m_robots, obj) 
    {
        robot = (Robot *) obj;
        posx = MIN(m_tilemap->getMapSize().width * m_tilemap->getTileSize().width - robot->getCenterToSides()
                    , MAX(robot->getCenterToSides(), robot->getDesiredPos().x));

        posy = MIN(3 * m_tilemap->getTileSize().height + robot->getcenterToBottom()
                    , MAX(robot->getcenterToBottom(), robot->getDesiredPos().y));

        robot->setPosition(ccp(posx, posy));
    }

    reorderActors();
}

void GameLayer::setViewPointCenter( CCPoint pos )
{
    //int x = MAX(pos.x, SCREEN.width/2);
    //int y = MAX(pos.y, SCREEN.height/2);
    //
    //x = MIN(x, (m_tilemap->getMapSize().width * m_tilemap->getTileSize().width)
    //            - SCREEN.width/2);
    //y = MIN(y, (m_tilemap->getMapSize().height * m_tilemap->getTileSize().height)
    //            - SCREEN.height/2);

    //CCPoint actualPos = ccp(x, y);
    //CCPoint centerOfView = ccp(SCREEN.width/2, SCREEN.height/2);
    //CCPoint viewPoint = ccpSub(centerOfView, actualPos);
    //setPosition(viewPoint);
}

void GameLayer::initRobots()
{
    int robotCount = 50;
    m_robots = CCArray::create();
    m_robots->retain();

    for (int i = 0; i < robotCount; i++)
    {
        Robot *robot = Robot::create();
        m_actors->addChild(robot);
        m_robots->addObject(robot);

        int minx = SCREEN.width + robot->getCenterToSides();
        int maxx = m_tilemap->getMapSize().width * m_tilemap->getTileSize().width - robot->getCenterToSides();
        int miny = robot->getcenterToBottom();
        int maxy = 3 * m_tilemap->getTileSize().height + robot->getcenterToBottom();

        robot->setScaleX(-1);
        robot->setPosition(ccp(random_range(minx, maxx), random_range(miny, maxy)));
        robot->setDesiredPos(robot->getPosition());

        robot->idle();
    }
}

void GameLayer::reorderActors()
{
    ActionSprite *sprite;
    CCObject *obj;
    CCARRAY_FOREACH(m_actors->getChildren(), obj)
    {
        sprite = (ActionSprite *) obj;
        m_actors->reorderChild(sprite, m_tilemap->getMapSize().height * m_tilemap->getTileSize().height - sprite->getPositionY());
    }
}

void GameLayer::updateRobots( float dt )
{
    CC_ASSERT(m_hero);


    int alive = 0;
    Robot *robot;
    CCObject *obj;
    float distanceSQ;
    int randomChoice = 0;
    CCARRAY_FOREACH(m_robots, obj) 
    {
        robot = (Robot *) obj;
        robot->update(dt);
        if (robot->getActionState() != kActionStateKnockedOut) 
        {
            //1
            alive++;

            //2
            if (CURTIME > robot->getNextDecisionTime()) 
            {
                distanceSQ = ccpDistanceSQ(robot->getPosition(), m_hero->getPosition());

                //3
                if (distanceSQ <= 50 * 50) 
                {
                    robot->setNextDecisionTime(CURTIME + frandom_range(0.1, 0.5));

                    randomChoice = random_range(0, 1);

                    // robot make the chose : attack hero or not
                    if (randomChoice == 0) 
                    {
                        if (m_hero->getPositionX() > robot->getPositionX()) 
                        {
                            robot->setScaleX(1.0);
                        } else 
                        {
                            robot->setScaleX(-1.0);
                        }

                        //4
                        robot->attack();
                        if (robot->getActionState() == kActionStateAttack) 
                        {
                            if (fabsf(m_hero->getPositionY() - robot->getPositionY()) < 10) 
                            {
                                if (CCRect::CCRectIntersectsRect(m_hero->getHitBox().actual
                                            , robot->getAttackBox().actual)) 
                                {
                                    m_hero->hurtWithDamage(robot->getDamage());

                                    //end game checker here
                                    if (m_hero->getActionState() == kActionStateKnockedOut && m_hud->getChildByTag(5) == NULL)
                                    {
                                        endGame();
                                    }
                                }
                            }
                        }
                    }
                    else 
                    {
                        robot->idle();
                    }
                } 
                else if (distanceSQ <= SCREEN.width * SCREEN.width) 
                {
                    //5
                    robot->setNextDecisionTime(CURTIME + frandom_range(0.5, 1.0));

                    randomChoice = random_range(0, 1);
                    if (randomChoice == 0) 
                    {
                        CCPoint moveDirection = ccpNormalize(
                            ccpSub(m_hero->getPosition(), robot->getPosition()));
                        robot->walkWithDirection(moveDirection);
                    } 
                    else 
                    {
                        robot->idle();
                    }
                }
            }
        }
    }

    //end game checker here
    if (alive == 0 && m_hud->getChildByTag(5) == NULL)
    {
        endGame();
    }

}

void GameLayer::endGame()
{
    CCLabelTTF *restartLabel = CCLabelTTF::create("restart", "Arial", 30);
    CCMenuItemLabel *restartItem = CCMenuItemLabel::create(restartLabel
        , this, menu_selector(GameLayer::restartGame));
    CCMenu *menu = CCMenu::create(restartItem, NULL);
    menu->setPosition(CENTER);
    menu->setTag(5);
    m_hud->addChild(menu, 5);
}

void GameLayer::restartGame( CCObject* )
{
    CCDirector::sharedDirector()->replaceScene(GameScene::create());
}

void GameLayer::loadMusic()
{
    SimpleAudioEngine *engine = SimpleAudioEngine::sharedEngine();
    engine->preloadBackgroundMusic(gDataMrg->cstringForKeyInLevelData("latin_industries"));
    engine->playBackgroundMusic(gDataMrg->cstringForKeyInLevelData("latin_industries"));

    engine->preloadEffect(gDataMrg->cstringForKeyInLevelData("pd_hit0"));
    engine->preloadEffect(gDataMrg->cstringForKeyInLevelData("pd_hit1"));
    engine->preloadEffect(gDataMrg->cstringForKeyInLevelData("pd_herodeath"));
    engine->preloadEffect(gDataMrg->cstringForKeyInLevelData("pd_botdeath"));
}

void GameLayer::draw()
{
    // bg
    //glColor4f(0,0,0,1.0);
    //glLineWidth(10);
    //ccDrawLine(ccp(0, SCREEN.height), ccp(SCREEN.width/2, SCREEN.height));

    // line
    glColor4f(1,1,0,1.0);
    glLineWidth(20);
    ccDrawLine(ccp(0, SCREEN.height)
            , ccp((SCREEN.width*m_hero->getHitPoints())/(2*m_hero->getHitPointsConst()), SCREEN.height));

    CCLayer::draw();
}
