#include "GameScene.h"
#include "Defines.h"

GameScene::GameScene()
    : m_gameLayer(0)
    , m_hudLayer(0)
{

}


bool GameScene::init()
{
    bool bRet = false;
    do 
    {
        setGameLayer(GameLayer::create(0));
        setHudLayer(HudLayer::create());

        addChild(m_gameLayer, 0);
        addChild(m_hudLayer, 1);

        // GameLayer has interface
        // HudLayer has interface
        // GameScene Assembly GameLayer and HudLayer .. good ..
#ifdef USING_FD
        m_hudLayer->getDPad()->didChangeDirectionToDelegate = MakeDelegate(m_gameLayer, &GameLayer::simpleDPadDidChangeDirectionTo);
        m_hudLayer->getDPad()->isHoldingDirectionDelegate = MakeDelegate(m_gameLayer, &GameLayer::simpleDPadIsHoldingDirection);
        m_hudLayer->getDPad()->dpadTouchEnded = MakeDelegate(m_gameLayer, &GameLayer::simpleDPadTouchEnded);
#else
        m_hudLayer->getDPad()->setDelegate(m_gameLayer);
#endif
        m_gameLayer->setHud(m_hudLayer);
        bRet = true;
    } while (0);
    return bRet;
}


