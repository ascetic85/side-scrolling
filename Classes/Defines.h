#pragma once
#include "cocos2d.h"
USING_NS_CC;

// 1 - convenience measurements
#define SCREEN CCDirector::sharedDirector()->getWinSize()
#define CENTER ccp(SCREEN.width/2, SCREEN.height/2)
#define CURTIME getTimeTick() /*CACurrentMediaTime() (s) */

#define USEC_SECOND 1000000
#define MICRO_SECOND 1000

inline float getTimeTick() 
{
    timeval time;
    gettimeofday(&time, NULL);
    
    return (time.tv_sec + time.tv_usec / USEC_SECOND); // (s)

    unsigned long millisecs = (time.tv_sec * MICRO_SECOND) + (time.tv_usec / MICRO_SECOND);
    return (float) millisecs;
}

// 2 - convenience functions
#define random_range(low,high) (rand()%(high-low+1))+low
#define frandom (float)rand()/UINT64_C(0x100000000)
#define frandom_range(low,high) ((high-low)*frandom)+low

// 3 - enumerations
typedef enum _ActionState {
    kActionStateNone = 0,
    kActionStateIdle,
    kActionStateAttack,
    kActionStateWalk,
    kActionStateHurt,
    kActionStateKnockedOut
} ActionState;

// 4 - structures
typedef struct _BoundingBox {
    // on the other hand, is the rectangle as it is located in world space. 
    // As the sprite moves, so does the actual rectangle. Think of it as the 
    // location of the bounding box as the GameLayer sees it.
    CCRect actual;

    //is the rectangle local to each individual sprite, and never changes once it is set.
    // Think of it as the internal location of the bounding box as the sprite sees it.
    CCRect original; 
} BoundingBox;