#include "CCHelper.h"

namespace CCH
{
    CCSprite * spriteWithSpriteFrameNameOrFile(const char *nameOrFile)
    {
        CCSpriteFrame *frame = gSpriteFrameCache->spriteFrameByName(nameOrFile);
        if (frame)
        {
            return CCSprite::createWithSpriteFrame(frame);
        }
        return CCSprite::create(nameOrFile);
    }

    CCArray * frameArrayHelper(const char *name, int capacity)
    {
        CCArray *frames = CCArray::create();
        for (int i = 0; i< capacity; i++)
        {
            CCString *idex = CCString::createWithFormat("%s_%02d.png", name, i);
            CCSpriteFrame *frame = gSpriteFrameCache->spriteFrameByName(idex->getCString());
            CC_ASSERT(frame && "frame is not in SpriteFrameCache");
            frames->addObject(frame);
        }
        return frames;
    }

    CCAnimation * animationHelper(const char *name, int capacity, float delay)
    {
        CCArray *frames = frameArrayHelper(name, capacity);
        return CCAnimation::createWithSpriteFrames(frames, delay);
    }

    CCAnimate * animateHelper(const char *name, int capacity, float delay)
    {
        return CCAnimate::create(animationHelper(name, capacity, delay));
    }

    CCRepeatForever * foreverAnimateHelper(const char *name, int capacity, float delay)
    {
        return CCRepeatForever::create(animateHelper(name, capacity, delay));
    }
}