#include "LevelDataMgr.h"

const char *leveldataStr = "leveldata";

LevelDataMgr::LevelDataMgr(void)
    : m_levelDict(0)
    , m_heroDict(0)
{
}


LevelDataMgr::~LevelDataMgr(void)
{
}

LevelDataMgr* LevelDataMgr::sharedMrg()
{
    static LevelDataMgr * mgr = NULL;
    if (!mgr)
    {   
        mgr = new LevelDataMgr();
    }
    return mgr;
}

void LevelDataMgr::readLevel( int level )
{
    CCString *levelStr = CCString::createWithFormat("level%d.plist", level);
    CCDictionary *dict = CCDictionary::createWithContentsOfFileThreadSafe(levelStr->getCString());
    setLevelDict(dict);
}

const char * LevelDataMgr::cstringForKeyInLevelData( const char *key )
{
    CCDictionary *dict = (CCDictionary*) m_levelDict->objectForKey(leveldataStr);
    return dict->valueForKey(key)->getCString();
}

const char * LevelDataMgr::cstringForKeyOfHero( const char *key )
{
    CC_ASSERT(m_heroDict);
    return m_heroDict->valueForKey(key)->getCString();
}

void LevelDataMgr::readHero( const char *hero )
{
    m_heroDict = CCDictionary::createWithContentsOfFileThreadSafe(hero);
}

const CCString * LevelDataMgr::valueForKey( const char *key )
{
    CC_ASSERT(m_heroDict);
    return m_heroDict->valueForKey(key);
}
