#pragma once

#include "cocos2d.h"
USING_NS_CC;

// hero_00.png,hero_01.png,hero_02.png,hero_03.png
// @name: hero
// @capacity: 4
// @delay: ..

CCArray* framesArrayHelper(const char *name, int capacity);

CCAnimation *animationHelper(const char *name, int capacity, float delay);

CCAnimate *animateHelper(const char *name, int capacity, float delay);

CCRepeatForever * foreverAnimateHelper(const char *name, int capacity, float delay);
