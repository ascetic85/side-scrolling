#include "ActionSprite.h"
#include "SimpleAudioEngine.h"
#include "LevelDataMgr.h"

using namespace CocosDenshion;

ActionSprite::ActionSprite()
    : m_idleAction(0)
    , m_attackAction(0)
    , m_walkAction(0)
    , m_hurtAction(0)
    , m_knockedOutAction(0)
{
}

ActionSprite::~ActionSprite()
{
    CC_SAFE_RELEASE_NULL(m_idleAction);
    CC_SAFE_RELEASE_NULL(m_attackAction);
    CC_SAFE_RELEASE_NULL(m_walkAction);
    CC_SAFE_RELEASE_NULL(m_hurtAction);
    CC_SAFE_RELEASE_NULL(m_knockedOutAction);
}

void ActionSprite::idle()
{
    if (m_actionState != kActionStateIdle)
    {
        stopAllActions();
        runAction(getIdleAction());
        m_actionState = kActionStateIdle;
        setVelocity(ccp(0, 0));
    }
}

void ActionSprite::attack()
{
    if (m_actionState == kActionStateIdle 
        || m_actionState == kActionStateAttack
        || m_actionState == kActionStateWalk)
    {
        stopAllActions();
        runAction(m_attackAction);
        m_actionState = kActionStateAttack;
    }
}

void ActionSprite::hurtWithDamage( float damage )
{
    if (getActionState() != kActionStateKnockedOut)
    {
        int randomSound = random_range(0, 1);
        SimpleAudioEngine::sharedEngine()->playEffect(
            gDataMrg->cstringForKeyInLevelData(
                CCString::createWithFormat("pd_hit0", randomSound)->getCString()));
        stopAllActions();
        runAction(m_hurtAction);
        setActionState(kActionStateHurt);
        m_hitPoints -= damage;

        if (m_hitPoints <= 0)
        {
            knockout();
        }
    }
}

void ActionSprite::knockout()
{
    stopAllActions();
    runAction(getKnockedOutAction());
    setHitPoints(0.0);
    setActionState(kActionStateKnockedOut);
}

void ActionSprite::walkWithDirection( CCPoint direction )
{
    if (m_actionState == kActionStateIdle)
    {
        stopAllActions();
        runAction(m_walkAction);
        m_actionState = kActionStateWalk;
    }

    if (m_actionState == kActionStateWalk)
    {
        setVelocity(ccp(direction.x * m_walkSpeed, direction.y * m_walkSpeed));
        if (getVelocity().x >= 0 ) 
        {
            setScaleX(1.0);
        }
        else
        {
            setScaleX(-1.0);
        }
    }
}

void ActionSprite::update( float dt )
{
    if (getActionState() == kActionStateWalk)
    {
        setDesiredPos(ccpAdd(getPosition(), ccpMult(getVelocity(), dt)));
    }
}

BoundingBox ActionSprite::createBoundingBoxWithOrigin( CCPoint origin, CCSize size )
{
    BoundingBox boundingBox;
    boundingBox.original.origin = origin;
    boundingBox.original.size = size;
    boundingBox.actual.origin = ccpAdd(getPosition(), ccp(boundingBox.original.origin.x, boundingBox.original.origin.y));
    boundingBox.actual.size = size;
    return boundingBox;
}

void ActionSprite::setPosition( CCPoint position )
{
    CCSprite::setPosition(position);

    transformBoxes();
}

// bug: when hero face left, can not hurt robot
void ActionSprite::transformBoxes()
{
    m_hitBox.actual.origin = ccpAdd(getPosition()
                            , ccp(m_hitBox.original.origin.x, m_hitBox.original.origin.y));
    m_hitBox.actual.size = CCSizeMake(m_hitBox.original.size.width * fabsf(getScaleX())
                            ,  m_hitBox.original.size.height * fabsf(getScaleY()));

    m_attackBox.actual.origin = ccpAdd(getPosition()
                            , ccp(m_attackBox.original.origin.x, m_attackBox.original.origin.y));
    m_attackBox.actual.size = CCSizeMake(m_attackBox.original.size.width * fabsf(getScaleX())
                            , m_attackBox.original.size.height * fabsf(getScaleY()));
}

