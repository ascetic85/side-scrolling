#include "HudLayer.h"
#include "JoyStick.h"

HudLayer::HudLayer(void)
{
}


HudLayer::~HudLayer(void)
{
}

bool HudLayer::init()
{
    bool bRet = false;
    do 
    {
        CC_BREAK_IF(!CCLayer::init());

        m_dpad = SimpleDPad::create("Sprites/pd_dpad.png", 64);
        m_dpad->setPosition(ccp(64,64));
        m_dpad->setOpacity(100);
        addChild(m_dpad);

        bRet = true;
    } while (0);
    return bRet;
}
