#pragma once

#include "ActionSprite.h"

class Hero : public ActionSprite
{
public:
    virtual bool init();

    CREATE_FUNC(Hero);
};