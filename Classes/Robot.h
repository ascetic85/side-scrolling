#pragma once

#include "ActionSprite.h"

class Robot : public ActionSprite
{
    CC_SYNTHESIZE_PASS_BY_REF(double, m_nextDecisionTime, NextDecisionTime);

public:
    virtual bool init();
    CREATE_FUNC(Robot);
};